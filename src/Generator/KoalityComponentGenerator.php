<?php

namespace Drupal\koality_theme_generator\Generator;

use Drupal\Console\Core\Generator\Generator;
use Drupal\Console\Extension\Manager;
use Drupal\Component\Serialization\Yaml;
use Drupalfinder\DrupalFinder;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class KoalityComponentGenerator.
 */
class KoalityComponentGenerator extends Generator
{


  /**
   * @var Manager
   */
  protected $extensionManager;

  /**
   * AuthenticationProviderGenerator constructor.
   *
   * @param Manager $extensionManager
   */
  public function __construct(
    Manager $extensionManager
  )
  {
    $this->extensionManager = $extensionManager;
  }

  /**
   * {@inheritdoc}
   */
  public function generate(array $parameters)
  {
    $dir = $parameters['dir'];
    $breakpoints = $parameters['breakpoints'];
    $machine_name = $parameters['machine_name'];
    $parameters['type'] = 'theme';

    $dir = ($dir == '/' ? '' : $dir) . '/' . $machine_name;
    if (file_exists($dir)) {
      if (!is_dir($dir)) {
        throw new \RuntimeException(
          sprintf(
            'Unable to generate the bundle as the target directory "%s" exists but is a file.',
            realpath($dir)
          )
        );
      }
      $files = scandir($dir);
      if ($files != ['.', '..']) {
        throw new \RuntimeException(
          sprintf(
            'Unable to generate the bundle as the target directory "%s" is not empty.',
            realpath($dir)
          )
        );
      }
      if (!is_writable($dir)) {
        throw new \RuntimeException(
          sprintf(
            'Unable to generate the bundle as the target directory "%s" is not writable.',
            realpath($dir)
          )
        );
      }
    }

    if ($parameters['base_theme_regions'] && $parameters['base_theme']) {
      $defaultRegions = Yaml::decode(file_get_contents($parameters['base_theme_path']));
      $parameters['base_theme_regions'] = $defaultRegions['regions'];
      $parameters['base_theme_regions_hidden'] = $defaultRegions['regions_hidden'];
    }

    $themePath = $dir . '/';
    $componentPath = $themePath . 'src/components/';
    $drupalFinder = new DrupalFinder();
    if ($drupalFinder->locateRoot(getcwd())) {
      $drupalRoot = $drupalFinder->getDrupalRoot();
      $module_template_dir = drupal_get_path('module', 'koality_theme_generator') . '/templates/';
      $this->addSkeletonDir($drupalRoot . '/' . $module_template_dir);

      $this->renderFile(
        'theme/sample-component/sample-component.twig',
        $componentPath . $machine_name . '/' .$machine_name . '.twig',
        $parameters
      );

      $this->renderFile(
        'theme/sample-component/sample-component.scss.twig',
        $componentPath . $machine_name . '/' .$machine_name . '.scss',
        $parameters
      );

      $this->renderFile(
        'theme/sample-component/sample-component.uipatterns.twig',
        $componentPath . $machine_name . '/' .$machine_name . '.ui_patterns.twig',
        $parameters
      );



      if ($breakpoints) {
        $this->renderFile(
          'theme/koality-breakpoints.yml.twig',
          $themePath . $machine_name . '.breakpoints.yml',
          $parameters
        );
      }
    }
    else {
      throw new \RuntimeException(
        sprintf(
          'Unable to generate the bundle as we cannot determine your Drupal Install path.'
        )
      );
    }
  }
}
